package com.accedo.onearena.Arenandroid

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import com.accedo.onearena.shared.Authentication
import com.google.android.material.textfield.TextInputEditText
import kotlin.math.log

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val loginButton: AppCompatButton = findViewById(R.id.loginButton)
        loginButton.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val email: TextInputEditText = findViewById(R.id.emailInputView)
        val password: TextInputEditText = findViewById(R.id.passwordInputView)

        val isSucces = Authentication().authenticate(username = email.text.toString(), password = password.text.toString())
        if (isSucces) {
            Log.d("login result", "login success")
        } else {
            Log.d("login result", "login failed")
        }
    }
}