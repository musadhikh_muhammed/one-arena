package com.accedo.onearena.shared.models

import kotlinx.serialization.Serializable

@Serializable
data class UserModel (
    var email: String,
    var password: String
)