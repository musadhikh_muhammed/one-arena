package com.accedo.onearena.shared.network

import com.accedo.onearena.shared.models.Rails
import io.ktor.client.*
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.json.Json

class OneArenaApi {

    companion object {
        private const val BASE_URL = "https://api.jsonbin.io"
        private const val RAIL_ENDPOINT = "/b/60138ccab41a937c6d539a53/6"
    }

    private val httpClient = HttpClient {
                install(JsonFeature) {
            val json = Json { ignoreUnknownKeys = true }
            serializer = KotlinxSerializer(json)
        }
    }

    suspend fun getAllRails(): Rails {
        return httpClient.get("$BASE_URL$RAIL_ENDPOINT")
    }

}