package com.accedo.onearena.shared.models

import kotlinx.serialization.Serializable

@Serializable
data class Rails(
    val rails: List<RailModel>
)