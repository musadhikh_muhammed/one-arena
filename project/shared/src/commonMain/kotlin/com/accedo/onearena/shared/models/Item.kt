package com.accedo.onearena.shared.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Item(
    @SerialName("id")
    val id: String,
    @SerialName("image")
    val image: String,
    @SerialName("openChallenges")
    val openChallenges: List<Challenge>,
    @SerialName("title")
    val title: String,
    @SerialName("upComingChallenges")
    val challenges: List<Challenge>,
    @SerialName("waitingChallenges")
    val waitingChallenges: List<Challenge>
)