package com.accedo.onearena.shared.models

import kotlinx.serialization.Serializable

@Serializable
data class Challenge(
    val challengeType: String,
    val date: String,
    val fanName: String,
    val id: String,
    val price: String,
    val proName: String
)