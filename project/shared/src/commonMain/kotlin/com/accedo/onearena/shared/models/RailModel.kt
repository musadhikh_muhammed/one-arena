package com.accedo.onearena.shared.models

import kotlinx.serialization.Serializable

@Serializable
data class RailModel (
    val id: String,
    val items: List<Item>,
    val title: String,
    val type: String
)