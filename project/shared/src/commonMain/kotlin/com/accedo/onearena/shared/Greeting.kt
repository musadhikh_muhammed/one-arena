package com.accedo.onearena.shared


class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}

class Authentication {
    private  var creds: HashMap<String, String> = HashMap()
    constructor() {
        creds.put("sample@value.com", "password1234")
    }
    fun authenticate(username: String, password: String): Boolean {
        return  true
//        val ps = creds[username]
//        return ps.equals(password)
    }
}

class Strings {
    companion object {
        val emailPlaceholder = "Enter your email id"
        val passwordPlaceholder = "Enter your Password"
        val emailLabelText = "Email"
        var passwordLabelText = "Password"
    }
}