package com.accedo.onearena.shared.repository

import com.accedo.onearena.shared.models.Rails
import com.accedo.onearena.shared.network.OneArenaApi

class OneArenaRepository() {

    private val api = OneArenaApi()

    @Throws(Exception::class)
    suspend fun getAllRails(): Rails {

        return api.getAllRails().also {
            // do something may be cache

        }

    }

}