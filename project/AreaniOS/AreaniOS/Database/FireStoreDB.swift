//
//  FireStoreDB.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 3/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import Firebase
import SwiftUI

class FireStoreDB: ObservableObject {
    static let storeDB = FireStoreDB()
    
    var games: [Game] = []
    var players: [Player] = []
    var user: User?
    
    var challengeNotifyCount = 0
    
    private let db = Firestore.firestore()
    private init() {}
    
    func configureApp(onComplete: @escaping () -> Void) {
        getGames {
            self.getPlayers {
                onComplete()
            }
        }
    }
    
    func getGames(onComplete: @escaping  () -> Void)  {
        db.collection("games").getDocuments { snapshot, error in
            guard let shot = snapshot else {
                onComplete()
                return
            }
            
            let games = shot.documents.compactMap { $0.data() }
            let result = JSONCodable.convertToReadable(data: games, to: [Game].self)
            switch result {
            case .failure(_ ): break
            case .success(let res): self.games = res
            }
            onComplete()
        }
    }
    
    func getPlayers(onComplete: @escaping  () -> Void) {
        db.collection("users")
            .whereField("userType", isEqualTo: UserType.pro.rawValue)
            .getDocuments { query, error in
            guard let shot = query else {
                onComplete()
                return
            }
            
            let players = shot.documents.compactMap { $0.data() }
            let result = JSONCodable.convertToReadable(data: players, to: [Player].self)
            switch result {
            case .failure(_ ): break
            case .success(let res): self.players = res
            }
            onComplete()
        }
    }
    
    func createChallenge(values: [String: String], onComeplete: @escaping () -> Void) {
        db.collection("challenges").addDocument(data: values) { error in
            onComeplete()
        }
    }
    
    func updateChallenge(challeng: [String: String], documentID: String) {
        db.collection("challenges").document(documentID).updateData(challeng)
    }
    
    
    func login(email: String, password: String, onComplete: @escaping (Bool) -> Void) {
        db.collection("users").whereField("email", isEqualTo: email).whereField("password", isEqualTo: password).getDocuments { query, error in
            guard let snapshot = query else {
                onComplete(false)
                return
            }
            
            if let userDoc = snapshot.documents.first {
                let res = JSONCodable.convertToReadable(data: userDoc.data(), to: User.self)
                switch res {
                case .success(let user):
                    self.user = user
                    UserViewModel.shared.saveUser(user: user)
                    onComplete(true)
                case .failure( _): onComplete(false)
                }
            }
        }
    }
    
    func listenToChallenges(onComplete: @escaping (Bool) -> Void) {
        let email = UserViewModel.shared.currentUser?.email ?? ""
        db.collection("challenges").whereField("challengedTo", isEqualTo: email).addSnapshotListener { query, error in
            guard let _ = query?.documentChanges else {
                onComplete(false)
                return
            }
            if self.challengeNotifyCount < 2 {
                onComplete(false)
                self.challengeNotifyCount += 1
            } else {
                onComplete(true)
            }
        }
    }
    
    func getAllChallenges(onComplete: @escaping (([ChallengeCreated]) -> Void)) {
        let email = UserViewModel.shared.currentUser?.email ?? ""
        db.collection("challenges").whereField("challengedTo", isEqualTo: email).getDocuments { query, error in
            guard let docs = query?.documents else {
                return
            }
            
            let cds = docs.compactMap { doc -> [String: Any] in
                var k = doc.data()
                k["documentID"] = doc.documentID
                return k
            }
            let res = JSONCodable.convertToReadable(data: cds, to: [ChallengeCreated].self)
            switch res {
            case .success(let c): onComplete(c)
            case .failure(let error): print(error)
                onComplete([])
            }
        }
    }
    
    func getChallengesFor(game: String, onComplete: @escaping (([ChallengeCreated]) -> Void)) {
        db.collection("challenges")
            .whereField("gameId", isEqualTo: game).getDocuments { q, e in
                guard let docs = q?.documents else {
                    return
                }
                
                let cds = docs.compactMap { doc -> [String: Any] in
                    var k = doc.data()
                    k["documentID"] = doc.documentID
                    return k
                }
                
                let res = JSONCodable.convertToReadable(data: cds, to: [ChallengeCreated].self)
                switch res {
                case .success(let c): onComplete(c)
                case .failure(let error): print(error)
                    onComplete([])
                }
            }
    }
}
