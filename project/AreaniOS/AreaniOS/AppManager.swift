//
//  AppManager.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import SwiftUI

enum AppState {
    case loading
    case configured
    case loggedin
}

class AppManager: ObservableObject {
    
    @Published var appState = AppState.loading
    
    static let shared = AppManager()
    
    func configureApp() {
        FireStoreDB.storeDB.configureApp {  
            self.appState = .loggedin
        }
    }
    
}
