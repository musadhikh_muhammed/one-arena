//
//  ChallengeViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 2/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class ChallengeViewModel: ObservableObject {
    
    func create(challenge: ChallengeCreated, onComplete: @escaping () -> Void)  {
        let res = JSONCodable.convertToReadable(data: challenge, to: [String: String].self)
        switch res {
        case .success(let data): FireStoreDB.storeDB.createChallenge(values: data, onComeplete: onComplete)
        case .failure(let err): print(err)
        }
    }
}
