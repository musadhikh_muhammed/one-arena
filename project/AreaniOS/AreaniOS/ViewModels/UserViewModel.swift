//
//  UserViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 4/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import SwiftUI

let kUserCreds = "userCreds"
let kLoggedin = "isLoggedin"

class UserViewModel: ObservableObject {
    static let shared = UserViewModel()
    
    @Published var user: User?
    
    func saveUser(user: User) {
        self.user = user
        
        let k = JSONCodable.encode(data: user)
        switch k {
        case .success(let d):
            UserDefaults.standard.setValue(d, forKey: kUserCreds)
            UserDefaults.standard.setValue(true, forKey: kLoggedin)
        case .failure( _): UserDefaults.standard.removeObject(forKey: kUserCreds)
        }
    }
    
    func logout() {
        UserDefaults.standard.removeObject(forKey: kUserCreds)
        user = nil
    }
    
    var isLoggedIn: Bool {
        return UserDefaults.standard.bool(forKey: kLoggedin)
    }
    
    var currentUser: User? {
        guard user == nil else {
            return user
        }
        if let data = UserDefaults.standard.value(forKey: kUserCreds) as? Data {
            let k = JSONCodable.decode(data: data, type: User.self)
            switch k {
            case .success(let u): self.user = u
            case .failure( _): break
            }
        }
         return user
    }
    
    var userFullName: String {
        return (currentUser?.firstName ?? "") + " " + (currentUser?.lastName ?? "")
    }
    
    var switchToUserName: String {
        guard let u = currentUser else {
            return ""
        }
        if u.email == "brad.pitt@gmail.com" {
            return "Adele John"
        } else {
            return "Brad Pitt"
        }
    }
    var switchToUserEmail: String {
        guard let u = currentUser else {
            return ""
        }
        if u.email == "brad.pitt@gmail.com" {
            return "adele.john@gmail.com"
        } else {
            return "brad.pitt@gmail.com"
        }
    }
}
