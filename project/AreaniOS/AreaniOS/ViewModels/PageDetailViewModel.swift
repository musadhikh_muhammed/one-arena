//
//  PageDetailViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 5/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class PageDetailViewModel: ObservableObject {
    @Published var challengeFetchCompleted = false
    @Published var allChallenges = [ChallengeCreated]()
    init() {
        
    }
    
    func fetChallenges(gameId: String)  {
        FireStoreDB.storeDB.getChallengesFor(game: gameId) { res in
            self.allChallenges = res
            self.challengeFetchCompleted = true
        }
    }
    
    func upComing() -> [ChallengeCreated] {
        return allChallenges.compactMap { $0.status == .accepted ? $0 : nil }
    }
    
    func waiting() -> [ChallengeCreated] {
        return allChallenges.compactMap { $0.status != .accepted ? $0 : nil }
    }
}
