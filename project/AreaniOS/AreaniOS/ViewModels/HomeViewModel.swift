//
//  HomeViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared

class HomeViewModel: ObservableObject {
    @Published var rails: [RailModel] = []
    let onearenaRepository: OneArenaRepository = OneArenaRepository()
    
    init() {
        getRails()
    }
    
    private func getRails() {
        onearenaRepository.getAllRails(completionHandler: { _rails, error in
            if let _rails = _rails {
                self.rails = _rails.rails
            } else {
                if let err = error {
                    print(err)
                }
            }
        })
    }
}
