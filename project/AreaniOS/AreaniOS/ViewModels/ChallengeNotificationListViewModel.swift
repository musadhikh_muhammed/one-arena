//
//  ChallengeNotificationListViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class ChallengeNotificationListViewModel: ObservableObject {
    @Published var challenges: [ChallengNotification] = []
    @Published var allChallenges: [ChallengeCreated] = []
    
    init() {
        fetchAllChallenges()
    }
    
    private func fetchAllChallenges() {
        FireStoreDB.storeDB.getAllChallenges { challenges in
            self.allChallenges = challenges
        }
    }
    
    func unread() -> [ChallengeCreated] {
        return allChallenges.compactMap { c -> ChallengeCreated? in
            if c.status == .unread {
                return c
            } else {
                return nil
            }
        }
    }
    
    func accepted() -> [ChallengeCreated] {
        return allChallenges.compactMap { c -> ChallengeCreated? in
            if c.status == .accepted {
                return c
            } else {
                return nil
            }
        }
    }
}
