//
//  BaseContainerViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 4/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class BaseContainerViewModel: ObservableObject {
    @Published var challengeNotificationRecieved = false
    
    init() {
        listenToChallengeChanges()
    }
    
    private func listenToChallengeChanges() {
        FireStoreDB.storeDB.listenToChallenges { hasUpdated in
            self.challengeNotificationRecieved = hasUpdated
        }
    }
}
