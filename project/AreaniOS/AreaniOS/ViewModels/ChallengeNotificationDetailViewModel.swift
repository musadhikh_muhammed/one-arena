//
//  ChallengeNotificationDetailViewModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 4/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

class ChallengeNotificationDetailViewModel {
    
    init() {}
    
    func updateStatus(newStatus: ChallengeStatus, challenge: ChallengeCreated) {
        
        let k = JSONCodable.convertToReadable(data: challenge, to: [String: String].self)
        switch k {
        case .success(var data):
            data["status"] = newStatus.rawValue
            FireStoreDB.storeDB.updateChallenge(challeng: data, documentID: challenge.documentID)
        case .failure( _): break
        }
    }
}
