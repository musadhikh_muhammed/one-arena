//
//  ChallengeCell.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 26/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct ChallengeCell: View {
    
    var challenge: ChallengeCreated
    
    var body: some View {
        HStack {
            VStack {
                ZStack {
                    Color.black
                    VStack {
                        Text(day())
                            .foregroundColor(.white)
                            .font(.system(size: 27, weight: .bold))
                        Text(month())
                            .foregroundColor(.white)
                            .font(.system(size: 17, weight: .regular))
                    }
                }
                .cornerRadius(10.0)
                .clipped()
                .frame(height: 78)
                Text(challenge.eventTime)
                    .foregroundColor(.challengeTime)
                    .font(.system(size: 14, weight: .regular))
            }
            .frame(width:78)
            Spacer()
            VStack {
                Text(challenge.createBy)
                    .font(.system(size: 14, weight: .regular))
                    .padding(.bottom, 5)
                Text("VS")
                    .font(.system(size: 16, weight: .regular))
                    .padding(.bottom, 5)
                Text(challenge.challengedName)
                    .font(.system(size: 14, weight: .regular))
            }
            Spacer()
            VStack {
                HStack {
                    Text(challenge.status == .accepted ? "$1000" : "$10")
                    Image(challenge.status == .accepted ? "PriceValid" : "PriceInvalid")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 30, height: 30)
                }
                Button(action: buttonAction, label: {
                    Text(challenge.status == .accepted ? "Watch" : "Donate")
                        .foregroundColor(.white)
                        .font(.system(size: 16, weight: .bold))
                        .padding(5)
                })
                .frame(width: 109, height: 42)
                .background(Color.blue)
                .cornerRadius(3.0)
            }
        }
        
    }
    
    private func buttonAction() {
        
    }
    
    
    private func month() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        if let date = formatter.date(from: challenge.eventDate) {
            formatter.dateFormat = "MMMM"
            let k = formatter.string(from: date)
            return k
        }
        
        return "March"
    }
    
    private func day() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        if let date = formatter.date(from: challenge.eventDate) {
            formatter.dateFormat = "dd"
            let k = formatter.string(from: date)
            return k
        }
        
        return "25"
    }
    
}
//
//struct ChallengeCell_Previews: PreviewProvider {
//    static var previews: some View {
//        ChallengeCell(challenge: Challenge(challengeType: "1", date: "25/05/2021 11:30am", fanName: "Dave", id: "John", price: "$1000", proName: "John"))
//    }
//}
