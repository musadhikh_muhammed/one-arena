//
//  ChallengeView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 26/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct ChallengeListView: View {
    var challenges: [ChallengeCreated]
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ForEach(challenges) { challenge in
                VStack {
                ChallengeCell(challenge: challenge)
                    .padding()
                Divider()
                    .background(Color.black)
                }
            }
        }
    }
}

//struct ChallengeListView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChallengeListView(challenges: [Challenge(challengeType: "1", date: "25/05/2021 11:30am", fanName: "Dave", id: "John", price: "$1000", proName: "upcoming"), Challenge(challengeType: "2", date: "26/05/2021 12:30pm", fanName: "James", id: "Rone", price: "$10,000", proName: "upcoming")])
//    }
//}



