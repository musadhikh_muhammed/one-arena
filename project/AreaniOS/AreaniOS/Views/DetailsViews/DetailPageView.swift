//
//  DetailPageView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 26/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct DetailPageView: View {
    let railItem: Item
    @ObservedObject var vm = PageDetailViewModel()
    @State var challengeType = 0
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        ScrollView {
            GeometryReader { geometry in
                ZStack {
                    Image(railItem.image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .clipped()
                    VStack {
                        HStack {}
                            .frame(height:40)
                        HStack {
                            Button(action: dismiss, label: {
                                Image(systemName: "chevron.backward")
                                    .foregroundColor(.white)
                                    .frame(width:40, height: 40)
                            })
                            
                            Spacer()
                        }
                        .padding(.leading, 5)
                        Spacer()
                    }
                }
            }
            .frame(height: 300)
            VStack {
                HStack {
                    Image(railItem.image)
                        .resizable()
                        .scaledToFit()
                        .frame(width:100)
                        .padding(.leading, 10)
                    VStack {
                        HStack {
                            Text("Cat Quest")
                            Spacer()
                        }
                        HStack {
                            Text("Game info")
                            Spacer()
                        }
                            
                        HStack {
                            Text("10 accepted challenges")
                                Spacer()
                        }
                        HStack {
                            Text("1 open challenge")
                            Spacer()
                        }
                            
                    }
                }
                HStack {
                    Text("Challenges")
                        .padding(.leading, 10)
                        .padding(.top)
                        .font(.system(size: 20, weight: .semibold))
                    Spacer()
                }
                if !vm.challengeFetchCompleted {
                    Text("Loading...")
                        .padding(.top, 20)
                } else if vm.allChallenges.isEmpty {
                    Text("No Challenges yet for this game")
                        .padding(.top, 20)
                } else {
                    Picker(selection: $challengeType, label: Text("Picker")) {
                        Text("Up Coming").tag(0)
                        Text("Waiting").tag(1)
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    
                    switch challengeType {
                    case 0: ChallengeListView(challenges: vm.upComing())
                    case 1: ChallengeListView(challenges: vm.waiting())
                    default: ChallengeListView(challenges: [])
                    }
                }
                
            }
        }
        .edgesIgnoringSafeArea(.top)
        .navigationBarHidden(true)
        .onAppear {
            vm.fetChallenges(gameId: railItem.id)
        }
    }
    
    private func dismiss() {
        self.presentation.wrappedValue.dismiss()
    }
}

struct DetailPageView_Previews: PreviewProvider {
    static var previews: some View {
        DetailPageView(railItem: Item(id:"", image: "", openChallenges: [], title: "", challenges: [], waitingChallenges: []))
    }
}
