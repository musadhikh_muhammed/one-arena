//
//  ChallengeNotificationListView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ChallengeNotificationListView: View {

    @Environment(\.presentationMode) var presentation
    @State private var notificationType = 0
    
    @ObservedObject var challengeNotificationViewModel = ChallengeNotificationListViewModel()
    
    var body: some View {
        ZStack {
            Color.white
            if !challengeNotificationViewModel.allChallenges.isEmpty {
                VStack {
                    Picker(selection: $notificationType, label: Text("Picker"), content: {
                        Text("All").tag(0)
                        Text("Unread").tag(1)
                        Text("Accepted").tag(2)
                    })
                    .pickerStyle(SegmentedPickerStyle())
                    
                    switch notificationType {
                    case 0: ChallengeNofiticationTableView(challenges: challengeNotificationViewModel.allChallenges)
                    case 1: ChallengeNofiticationTableView(challenges: challengeNotificationViewModel.unread())
                    case 2: ChallengeNofiticationTableView(challenges: challengeNotificationViewModel.accepted())
                    default: ChallengeNofiticationTableView(challenges: [])
                    }
                    Spacer()
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
    }
    
    var backButton: some View {
        Button(action: {
            presentation.wrappedValue.dismiss()
        }, label: {
            Image(systemName: "arrow.left")
                .foregroundColor(.black)
        })
    }
}

struct ChallengeNotificationListView_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeNotificationListView()
    }
}
