//
//  ChallengeNofiticationTableView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ChallengeNofiticationTableView: View {
    private(set) var challenges: [ChallengeCreated]
    
    @State var presentingModel = false

    var body: some View {
        ScrollView {
            ForEach(challenges) { challenge in
                VStack {
                    ChallengeNotificationCell(challenges: challenge)
                    Divider()
                }
                .padding(.leading)
                .padding(.trailing)
            }
        }
    }
}

//#if DEBUG
//struct ChallengeNofiticationTableView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChallengeNofiticationTableView(challenges: [ChallengNotification(id: "1", challengedBy: "Joe Tribbioni", createdAt: "20 JAN", shortDescription: "I would like to invite you on a challenge…", description: "", image: "s1")])
//    }
//}
//#endif
