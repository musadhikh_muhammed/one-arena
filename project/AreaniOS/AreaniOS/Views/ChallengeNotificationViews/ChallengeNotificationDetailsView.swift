//
//  ChallengeNotificationDetailsView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 28/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ChallengeNotificationDetailsView: View {
    @Binding var presentedAsModel: Bool
    var viewModel = ChallengeNotificationDetailViewModel()
    var challenge:ChallengeCreated
    
    var body: some View {
        ZStack {
            Color.clear
            VStack {
                ChallengeImageRowView(challenge: challenge)
                Text(challenge.intro)
                    .foregroundColor(.black)
                HStack {
                    Text("Challenge Date : \(challenge.eventDate)")
                        .font(.system(size: 16, weight: .bold))
                    Spacer()
                }
                .padding(.top)
                HStack {
                    Text("Time: \(challenge.eventTime)")
                        .font(.system(size: 16, weight: .bold))
                    Spacer()
                }
                .padding(.top, 3)
                Button(action: {
                    if challenge.status != .accepted {
                        viewModel.updateStatus(newStatus: .accepted, challenge: self.challenge)
                        presentedAsModel = false
                    }
                }, label: {
                    Text((challenge.status == .accepted ? "donate" : "accept challenge").uppercased())
                        .convertBlueGradientButton()
                })
                .padding(.top)
                Spacer()
            }
            .padding(EdgeInsets(top: 40, leading: 20, bottom: 20, trailing: 20))
        }
        .edgesIgnoringSafeArea(.bottom)
        .onAppear {
            if self.challenge.status == .unread {
                viewModel.updateStatus(newStatus: .pending, challenge: self.challenge)
            }
        }
    }
}

//struct ChallengeNotificationDetailsView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChallengeNotificationDetailsView(presentedAsModel: .constant(false), challenge: ChallengNotification(id: "1", challengedBy: "Joe Tribbioni", createdAt: "20 JAN", shortDescription: "I would like to invite you on a challenge…", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", image: "s1"))
//    }
//}
