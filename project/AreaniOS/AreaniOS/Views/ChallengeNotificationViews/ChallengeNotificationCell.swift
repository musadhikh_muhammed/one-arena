//
//  ChallengeNotificationCell.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ChallengeNotificationCell: View {
    @State private var presentModel = false
    
    private(set) var challenges: ChallengeCreated
    
    var body: some View {
        VStack {
            
            ChallengeImageRowView(challenge: challenges)
            VStack {
                HStack {
                    Text(challenges.intro)
                        .font(.system(size: 14, weight: .regular))
                    Spacer()
                }
                HStack {
                    Text("read more")
                        .foregroundColor(.blue)
                        .font(.system(size: 14, weight: .regular))
                    Spacer()
                }
            }
        }
        .onTapGesture {
            presentModel = true
        }
        .sheet(isPresented: $presentModel, content: {
            ChallengeNotificationDetailsView(presentedAsModel: $presentModel, challenge: challenges)
        })
    }
}

//struct ChallengeNotificationCell_Previews: PreviewProvider {
//    static var previews: some View {
//        ChallengeNotificationCell(challenge: ChallengNotification(id: "1", challengedBy: "Joe Tribbioni", createdAt: "20 JAN", shortDescription: "I would like to invite you on a challenge…", description: "", image: "s1"))
//    }
//}

struct ChallengeImageRowView: View {
    var challenge: ChallengeCreated
    
    var body: some View {
        HStack {
            Image("ChallengedBy")
                .resizable()
                .scaledToFit()
                .frame(width: 57)
            
            VStack {
                HStack {
                    Text(challenge.createBy)
                        .font(.system(size: 14, weight: .bold))
                    Spacer()
                }
                HStack {
                    Text(challenge.eventDate)
                        .font(.system(size: 14, weight: .regular))
                    Spacer()
                }
            }
            .padding(.leading, 5)
            .padding(.trailing, 5)
            
            HStack {
                AsyncImage(url: challenge.gameThumbnailUrl) {
                    Image(systemName: "photo")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 57)
                }
                .scaledToFit()
                .frame(width: 57)
            }
        }
    }
    
}
