//
//  ChallengeView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ChallengeView: View {
    @Environment(\.presentationMode) private var presentation
    
    private var viewModel = ChallengeViewModel()
    
    private let pString = "Enter your text here..."
    @State private var startEditing = false
    
    @State private var presentGamesList = false
    @State private var presentGamersList = false
    @State private var presentDates = false
    @State private var presentTimes = false
    
    @State private var selectedGame: Game?
    @State private var selectedPro: Player?
    @State private var selectedDate: Date = Date()
    @State private var selectedTime: Date = Date()
    @State private var introText: String = "Enter your text here..."
    
    init() {
        UITextView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        ZStack {
            Color.white
            VStack {
                headerBar
                HStack {
                    VStack {
                        challengeToButton
                            .frame(height: 40)
                        dateButton
                            .frame(height: 40)
                        timeButton
                            .frame(height: 40)
                    }
                    Spacer()
                    VStack {
                        gameSelectionButton
                    }
                }
                
                challengeIntroText
                
                textEditor
                .frame(height: 250)
                
                Spacer()
                
                createButton
                    .offset(y: startEditing ? -200 : 0)
                
                VStack {
                    Spacer()
                }
                
                if presentDates {
                    datePicker
                }
                
                if presentTimes {
                    timePicker
                }
            }
            Spacer()
        }
        .edgesIgnoringSafeArea(.all)
    }
}

//MARK:- View Components
extension ChallengeView {
    var headerBar: some View {
        VStack {
            HStack {
                Button(action: {
                    presentation.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "xmark")
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width:25, height: 25)
                        .padding(.all)
                })
                Spacer()
            }
            
            HStack {
                Text("Create")
                    .font(.system(size: 38, weight: .bold))
                Spacer()
            }
            .padding(.leading)
        }.padding(.top, 40)
    }
    
    var gameSelectionButton: some View {
        Button(action: {
            self.presentGamesList = true
        }, label: {
            VStack {
                
                AsyncImage(url: selectedGame?.thumbnailUrl) {
                    Image(systemName: "circle.grid.cross")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 58)
                        .cornerRadius(5)
                }
                .scaledToFit()
                .frame(width: 58)
                .cornerRadius(5)
                
                Text(selectedGame?.title ?? "Choose a game")
            }
        })
        .padding(.trailing)
        .sheet(isPresented: $presentGamesList) {
            ListPickerView(items: FireStoreDB.storeDB.games, title: "Games", selected: $selectedGame)
                
        }
    }
    
    var challengeToButton: some View {
        Button(action: {
            self.presentGamersList = true
        }, label: {
            HStack {
                Image(systemName: "person.fill")
                    .font(.system(size: 16, weight: .bold))
                Text(selectedPro?.title ?? "Select a player")
                Image(systemName: "chevron.down")
                    .font(.system(size: 16, weight: .bold))
                Spacer()
            }
        })
        .padding(.leading, 10)
        .sheet(isPresented: $presentGamersList) {
            ListPickerView(items: FireStoreDB.storeDB.players, title: "Players", selected: $selectedPro)
            
        }
    }
    
    var dateButton: some View {
        Button(action: {
            presentDates = true
            presentTimes = false
            startEditing = false
        }, label: {
            HStack {
                Image(systemName: "calendar")
                    .font(.system(size: 16, weight: .bold))
                Text(convertDate())
                Image(systemName: "chevron.down")
                    .font(.system(size: 16, weight: .bold))
                Spacer()
            }
        })
        .padding(.leading, 10)
    }
    
    var timeButton: some View {
        Button(action: {
            presentDates = false
            presentTimes = true
            startEditing = false
        }, label: {
            HStack {
                Image(systemName: "clock")
                    .font(.system(size: 16, weight: .bold))
                Text(convertTime())
                Image(systemName: "chevron.down")
                    .font(.system(size: 16, weight: .bold))
                Spacer()
            }
        })
        .padding(.leading, 10)
    }
    
    var challengeIntroText: some View {
        HStack {
            Text("Challenge Intro")
                .font(.system(size: 14, weight: .regular))
            Spacer()
        }
        .padding(.top, 10)
        .padding(.leading, 10)
    }
    
    var textEditor: some View {
        ZStack {
            Color.textEditorBG
            TextEditor(text: $introText)
                .foregroundColor(startEditing ? .black : .gray)
                .onTapGesture {
                    if introText == pString {
                        introText = ""
                        startEditing = true
                        presentDates = false
                        presentTimes = false
                    }
                }
                .background(Color.clear)
        }
        .padding(10)
    }
    
    var createButton: some View {
        VStack {
            Button(action: {
                startEditing = false
                presentTimes = false
                presentDates = false
                
                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                let user = UserViewModel.shared.currentUser
                
                let challenge = ChallengeCreated(id: user?.id ?? "1", createBy: (user?.firstName ?? "").appending(" ").appending((user?.lastName ?? "")), challengedTo: selectedPro?.email ?? "unknown", eventDate: convertDate(), eventTime: convertTime(), gameTitle: selectedGame?.title ?? "unknown game", gameId: selectedGame?.id ?? "1", gameThumbnailUrl: selectedGame?.thumbnailUrl ?? "", intro: introText, status: .unread, documentID: "", challengedName: ((selectedPro?.firstName ?? "") + " " + (selectedPro?.lastName ?? "")) )
                viewModel.create(challenge: challenge) {
                    presentation.wrappedValue.dismiss()
                }
                
                
            }, label: {
                Text("Create Challenge".uppercased())
                    .convertBlueGradientButton()
            })
            .padding()
        }
        .background(Color.white)
        .padding(.top)
        .padding(.bottom)
        
    }
    
    var datePicker: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    presentDates = false
                }, label: {
                    Text("Select")
                })
                .padding(.trailing)
            }
            .background(Color.white)
            .frame(height:30)
            DatePicker("", selection: $selectedDate,in: Date()..., displayedComponents: .date)
                .datePickerStyle(WheelDatePickerStyle())
                .background(Color.white)
        }
    }
    
    var timePicker: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    presentTimes = false
                }, label: {
                    Text("Select")
                })
                .padding(.trailing)
            }
            .background(Color.white)
            .frame(height:30)
            DatePicker("", selection: $selectedTime, displayedComponents: .hourAndMinute)
                .datePickerStyle(WheelDatePickerStyle())
                .background(Color.white)
        }
    }
}

//MARK:- Actions
extension ChallengeView {
    private func convertDate() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter.string(from: selectedDate)
    }
    
    private func convertTime() -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: selectedTime)
    }
}

#if DEBUG
struct ChallengeView_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeView()
    }
}
#endif
