//
//  CalendarView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct CalendarView: View {
    var body: some View {
        Text("Hello, Calendar!")
    }
}

struct CalendarView_Previews: PreviewProvider {
    static var previews: some View {
        CalendarView()
    }
}
