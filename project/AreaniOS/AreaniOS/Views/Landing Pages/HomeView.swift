//
//  HomeView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject private var homeViewModel: HomeViewModel
    @State var navigationBarActionType: NavigationBarButtonActionType = .none
    
    init(viewModel: HomeViewModel) {
        self.homeViewModel = viewModel
    }
    
    var body: some View {
        ZStack {
            Color.white
            VStack {
                ScrollView {
                    VStack {
                        ForEach(self.homeViewModel.rails) { rail in
                            if let type = RailType(rawValue: rail.type) {
                                switch type {
                                case .hero: HeroBanner(rail: rail)
                                    .frame(height:220)
                                case .category: CatlaneView(rail: rail)
                                    .frame(height: 140)
                                    .padding(.bottom, 20)
                                case .swimlane: SwimlaneView(rail: rail)
                                    .padding(.bottom, 10)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func getDestination() -> AnyView {
        if navigationBarActionType == .notification {
            return AnyView(ChallengeNotificationListView())
        } else {
            return AnyView(Text(""))
        }
    }
}

#if DEBUG
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(viewModel: HomeViewModel())
    }
}
#endif
