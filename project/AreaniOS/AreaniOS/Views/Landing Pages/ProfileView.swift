//
//  ProfileView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    private var userVM = UserViewModel.shared
    @State var currentUserName: String = UserViewModel.shared.userFullName

    var body: some View {
        VStack {
            Spacer()
            if userVM.isLoggedIn {
                imageView
                    .padding(.top, 20)
                
                Spacer()
                
                VStack {
                    switchButton
                    Spacer()
                }
                
            } else {
                loginButton
                Spacer()
            }
            
            Spacer()
        }
    }
}

extension ProfileView {
    var imageView: some View {
        VStack {
            HStack {
                Spacer()
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .foregroundColor(.gray)
                    .frame(width: 100, height: 100)
                Spacer()
            }
            
            if userVM.isLoggedIn {
                HStack {
                    Text("Logged In as \(currentUserName)")
                }
            }
            Spacer()
        }
    }
    
    var switchButton: some View {
        Button(action: {
            login()
        }, label: {
            Text("Switch to \(UserViewModel.shared.switchToUserName)")
        })
        .convertBlueGradientButton()
        .padding()
    }
    
    var loginButton: some View {
        Button(action: {
            if UserViewModel.shared.isLoggedIn {
                UserViewModel.shared.logout()
            } else {
                AppManager.shared.appState = .configured
            }
        }, label: {
            Text(UserViewModel.shared.isLoggedIn ? "Logout" : "Login")
        })
        .convertBlueGradientButton()
        .padding()
    }
}

extension ProfileView {
    private func login() {
        let email = UserViewModel.shared.switchToUserEmail
        UserViewModel.shared.logout()
        FireStoreDB.storeDB.challengeNotifyCount = 0
        FireStoreDB.storeDB.login(email: email, password: "password1234") { isSuccess in
            AppManager.shared.appState = .loggedin
            self.currentUserName = userVM.userFullName
        }
        
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
