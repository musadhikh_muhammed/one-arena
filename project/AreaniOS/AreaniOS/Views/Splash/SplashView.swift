//
//  SplashView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct SplashView: View {
    
    var body: some View {
        SplashLogo()
    }
}
#if DEBUG
struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
#endif

fileprivate struct SplashLogo: View {
    var body: some View {
        VStack(alignment: .center, spacing: 10) {
            Image("Logo")
                .padding(.bottom, 28)
            Text("One Arena")
                .font(.system(size: 38, weight: .bold, design: .default))
                .padding(.bottom, 20)
            Text("Accept the challenge")
                .font(.system(size: 16, weight: .medium, design: .default))
                .foregroundColor(Color(red: 183.0/255, green: 188/255.0, blue: 200/255.0))
        }
    }
}
