//
//  AppRootView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct AppRootView: View {
    @ObservedObject private(set) var appManager: AppManager
    
    var body: some View {
        Group {
            switch appManager.appState {
            case .loading: SplashView()
            case .configured: LoginUIView()
            case .loggedin: RootTabView()
            }
        }
    }
}

#if DEBUG
struct AppRootView_Previews: PreviewProvider {
    static var previews: some View {
        AppRootView(appManager: AppManager.shared)
    }
}
#endif
