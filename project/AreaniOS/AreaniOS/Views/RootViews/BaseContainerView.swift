//
//  BaseContainerView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct BaseContainerView<Content>: View where Content: View {
    private(set) var content: Content
    
    @State private var navigateToNotificationList = false
    @State var challengeUdpatesReceived = false
    
    @ObservedObject private var viewModel = BaseContainerViewModel()
    
    public init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }
    
    var body: some View {
        VStack {
            ZStack {
                Color.white
                VStack {
                    HStack { Spacer() }
                        .frame(height:60)
                    HStack {
                        Spacer()
                        search
                        notification
                    }
                    .frame(height: 30)
                    titleView
                    Spacer()
                }
            }
            .frame(height: 120)
            Spacer()
            content
        }
        .edgesIgnoringSafeArea(.top)
        
    }
    
    private func searchButtonAction() {
        
    }
    
    private func notificationButtonAction() {
        self.navigateToNotificationList = true
        viewModel.challengeNotificationRecieved = false
    }
}

extension BaseContainerView {
    var titleView: some View {
        HStack {
            Text("One Arena")
                .font(.system(size: 32, weight: .bold, design: .default))
                .padding(.leading)
                .padding(.bottom)
            Spacer()
        }
        .frame(height:40)
    }
    
    var search: some View {
        NavigationLink(
            destination: Text("Destination"),
            label: {
                Button(action: searchButtonAction) {
                    Image("SearchIcon")
                }
            })
            .frame(width: 30, height: 30)
            .padding(10)
    }
    
    var notification: some View {
        NavigationLink(
            destination: ChallengeNotificationListView()
                .navigationBarTitle("Challenges"),
            isActive: $navigateToNotificationList,
            label: {
                Button(action: notificationButtonAction) {
                    Image(systemName: "bell.badge")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 25)
                        .foregroundColor(viewModel.challengeNotificationRecieved ? .red : .black)
                }
                .frame(width: 30, height: 30)
                .padding(.trailing,10)
            })
    }
}

struct BaseContainerView_Previews: PreviewProvider {
    static var previews: some View {
        BaseContainerView {
            Text("What...")
            Spacer()
        }
    }
}
