//
//  RootTabView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct RootTabView: View {
    @State var presentModel = false
    @State private var selectedIndex = 0
    private var homeViewModel = HomeViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                ZStack {
                    Color.white
                    BaseContainerView {
                        if selectedIndex == 0 {
                            HomeView(viewModel: homeViewModel)
                                .navigationBarHidden(true)
                        } else if selectedIndex == 1 {
                            FavouriteView()
                                .navigationBarHidden(true)
                        } else if selectedIndex == 3 {
                            CalendarView()
                                .navigationBarHidden(true)
                        } else if selectedIndex == 4 {
                            ProfileView()
                                .navigationBarHidden(true)
                        }
                    }
                    
                }
                .padding(.bottom, -10)
                .fullScreenCover(isPresented: $presentModel, content: {
                    ChallengeView()
                })
                Spacer()
                CustomTabView(selectedIndex: $selectedIndex, presentModel: $presentModel)
            }
            .background(Color.tabForeground)
            .edgesIgnoringSafeArea(.bottom)
        }
    }
}


#if DEBUG
struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        RootTabView()
    }
}
#endif
