//
//  CustomTabView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 29/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct CustomTabView: View {
    
    @Binding var selectedIndex: Int
    @Binding var presentModel: Bool
    
    var body: some View {
        VStack {
            HStack {
                CustomTabItemButton(
                    selectedIndex: $selectedIndex,
                    tag: 0,
                    image: "gamecontroller",
                    title: "Home")
                
                Spacer()
                
                CustomTabItemButton(
                    selectedIndex: $selectedIndex,
                    tag: 1,
                    image: "star",
                    title: "Favourite")
               
                
                Spacer()
                
                
                
                Button(action: {
                    presentModel = true
                },
                label: {
                    ZStack {
                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .foregroundColor(Color.tabItemTitle)
                            .offset(y:-20)
                            .frame(height: 42)
                        VStack {
                            Spacer()
                            Text("Challenge")
                                .font(.system(size: 10, weight: .regular))
                                .foregroundColor(Color.tabItemTitle)
                                .padding(.top, 1)
                        }
                        
                    }
                    .frame(height: 42)
                })
                
                
                
                
                
                
                Spacer()
                
                CustomTabItemButton(
                    selectedIndex: $selectedIndex,
                    tag: 3,
                    image: "calendar",
                    title: "Calendar")
                
                Spacer()
                
                CustomTabItemButton(
                    selectedIndex: $selectedIndex,
                    tag: 4,
                    image: "person.crop.circle",
                    title: "Profile")
            }
            .padding(.horizontal, 25)
            .padding(.top, 10)
            Spacer()
        }
        .frame(height:85)
        .background(Color.tabForeground)
    }
}


struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabView(selectedIndex: .constant(0), presentModel: .constant(false))
    }
}


struct CustomTabItem: View {
    private(set) var isSelected: Bool
    private(set) var image: String
    private(set) var title: String
    
    var body: some View {
        VStack {
            Image(systemName: image)
                .resizable()
                .scaledToFit()
                .foregroundColor(self.isSelected ? Color.tabItemTitleSelected : Color.tabItemTitle)
            Text(title)
                .font(.system(size: 10, weight: .regular))
                .foregroundColor(self.isSelected ? Color.tabItemTitleSelected : Color.tabItemTitle)
                .padding(.top, 0.5)
        }
        .frame(height: 42)
    }
}

struct CustomTabItemButton: View {
    @Binding var selectedIndex: Int
    private(set) var tag: Int = 0
    private(set) var image: String
    private(set) var title: String
    
    var body: some View {
        Button(action: {
            self.selectedIndex = tag
        }, label: {
            CustomTabItem(
                isSelected: self.selectedIndex == tag,
                image: image,
                title: title)
                .padding(.top, 1)
                .padding(.bottom, 1)
        })
        
    }
}
