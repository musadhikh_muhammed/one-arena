//
//  SwimlaneView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct SwimlaneView: View {
    let rail: RailModel
    
    var body: some View {
        ZStack {
            VStack {
                SwimlaneHeaderView(title: rail.title)
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(rail.items) { item in
                            SwimlaneCellView(item: item)
                                .frame(width: 100, height: 120)
//                                .padding(.trailing, 5)
                                .clipped()
                        }
                        .padding(.leading, 15)
                    }
                    
                }
            }
        }
    }
}

struct CatlaneView: View {
    let rail: RailModel
    
    var body: some View {
        ZStack {
            HStack {
                ForEach(rail.items) { item in
                    CategoryCellView(item: item)
//                        .frame(width: 100, height: 100)
                }
            }
//            .padding(.leading, 20)
//            .padding(.trailing, 20)
        }
    }
}

struct SwimlaneView_Previews: PreviewProvider {
    static var previews: some View {
        CatlaneView(rail: RailModel(id: "1", items: [Item(
                                                        id:"1",
                                                        image: "c1",
                                                        openChallenges: [],
                                                        title: "Open Challenge",
                                                        challenges: [],
                                                        waitingChallenges: []), Item(
                                                            id:"1",
                                                            image: "c2",
                                                            openChallenges: [],
                                                            title: "",
                                                            challenges: [],
                                                            waitingChallenges: []), Item(
                                                                id:"1",
                                                                image: "c3",
                                                                openChallenges: [],
                                                                title: "",
                                                                challenges: [],
                                                                waitingChallenges: []), Item(
                                                                    id:"4",
                                                                    image: "c4",
                                                                    openChallenges: [],
                                                                    title: "",
                                                                    challenges: [],
                                                                    waitingChallenges: [])], title: "", type: ""))
    }
}
