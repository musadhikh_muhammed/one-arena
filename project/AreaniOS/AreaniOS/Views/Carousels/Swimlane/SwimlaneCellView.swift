//
//  SwimlaneCellView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct SwimlaneCellView: View {
    var item: Item
    
    var body: some View {
        NavigationLink(destination: DetailPageView(railItem: item)) {
            ZStack {
                VStack {
                    Image(item.image)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 100, height: 100)
                        .cornerRadius(5)
                    HStack {
                        Text(item.title)
                            .foregroundColor(.swimlaneTitle)
                            .font(.system(size: 12, weight: .medium, design: .default))
                        Spacer()
                    }
                }
                .frame(width: 100, height: 100)
                VStack {
                    HStack {
                        Spacer()
                        Image("YellowBG")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 20, height: 20)
                            .padding(.top, 5)
                            .padding(.trailing, 5)
                    }
                    Spacer()
                }
            }
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct CategoryCellView: View {
    var item: Item
    
    var body: some View {
        ZStack {
            VStack {
                Image(item.image)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 80, height: 80)
                Text(item.title)
                    .foregroundColor(.swimlaneTitle)
                    .font(.system(size: 10, weight: .medium, design: .default))
            }
            .frame(width: 80, height: 100)
        }
    }
}

struct SwimlaneCellView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryCellView(
            item: Item(
                id:"1",
                image: "c1",
                openChallenges: [],
                title: "Open Challenge",
                challenges: [],
                waitingChallenges: [])
        )
    }
}



