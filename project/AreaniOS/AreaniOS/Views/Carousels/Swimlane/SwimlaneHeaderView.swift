//
//  SwimlaneHeaderView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct SwimlaneHeaderView: View {
    var title: String
    var body: some View {
        HStack {
            Text(title)
                .padding(.leading, 20)
                .font(.system(size: 20, weight: .semibold, design: .default))
            Spacer()
        }
    }
}

struct SwimlaneHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        SwimlaneHeaderView(title: "Trending Games")
    }
}
