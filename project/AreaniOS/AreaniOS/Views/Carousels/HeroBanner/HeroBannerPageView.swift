//
//  HeroBannerPageView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared
extension Challenge : Identifiable { }
extension RailModel : Identifiable { }
extension Item : Identifiable { }

struct HeroBannerPageView: View {
    let items: [Item]
    
    var body: some View {
        TabView {
            ForEach(items) {item in
                ZStack {
                    HeroBannerCell(item: item)
                }
                .cornerRadius(5.0)
            }
            .padding(.all, 0)
        }
        .frame(width: UIScreen.main.bounds.size.width, height: 200)
        .tabViewStyle(PageTabViewStyle())
    }
}

#if DEBUG
struct HeroBannerPageView_Previews: PreviewProvider {
    static var previews: some View {
        HeroBannerPageView(items: [])
    }
}
#endif
