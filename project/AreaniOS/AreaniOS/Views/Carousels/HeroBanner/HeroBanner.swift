//
//  HeroBanner.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct HeroBanner: View {
    let rail: RailModel
    
    var body: some View {
        ZStack {
            ScrollView {
                LazyHStack {
                    HeroBannerPageView(items: rail.items)
                        .frame(height: 210)
                }
            }
        }
        
    }
}

#if DEBUG
struct HeroBanner_Previews: PreviewProvider {
    static var previews: some View {
        HeroBanner(rail: RailModel(
            id: "", items: [], title: "", type: ""
        ))
    }
}
#endif
