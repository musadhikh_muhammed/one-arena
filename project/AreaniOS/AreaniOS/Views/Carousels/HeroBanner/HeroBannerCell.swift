//
//  HeroBannerCell.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct HeroBannerCell: View {
    let item: Item
    var body: some View {
        Group {
            Image(item.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
        }
    }
}

struct HerBannerCell_Previews: PreviewProvider {
    static var previews: some View {
        HeroBannerCell(
            item: Item(
                id:"",
                image: "",
                openChallenges: [],
                title: "",
                challenges: [],
                waitingChallenges: [])
        )
    }
}
