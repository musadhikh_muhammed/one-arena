//
//  TextInputLayout.swift
//  AreaniOS
//
//  Created by Chan Pyae Aung on 23/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct TextInputLayout: View {
    
    private(set) var hint: String
    private(set) var textLabel: String
    private(set) var textContentType: UITextContentType
    private(set) var isSecured: Bool
    
    @Binding var user: UserModel
    
    var body: some View {
        VStack {
            HStack {
                Text(textLabel)
                    .font(.system(size: 16))
                    .foregroundColor(Color.black)
                Spacer()
            }
            if (isSecured) {
                SecureField(hint, text: $user.password)
                    .font(.system(size: 16))
            } else {
                TextField(hint, text: $user.email)
                    .font(.system(size: 16))
            }
        }
    }
}

struct TextInputLayout_Previews: PreviewProvider {
    static var previews: some View {
        TextInputLayout(hint: "blah", textLabel: "blah blah", textContentType: .name, isSecured: false, user: .constant(UserModel(email: "", password: "")))
    }
}
