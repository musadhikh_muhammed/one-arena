//
//  LoginInputView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 23/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct LoginInputView: View {
    
    @Binding private(set) var userModel: UserModel
    
    var body: some View {
        VStack {
            let texts = Strings.Companion()
            TextInputLayout(hint: texts.emailPlaceholder, textLabel: texts.emailLabelText, textContentType: .emailAddress, isSecured: false, user: $userModel)
                .padding(EdgeInsets(top: 27, leading: 27, bottom: 0, trailing: 0))
            
            TextInputLayout(hint: texts.passwordPlaceholder, textLabel: texts.passwordLabelText, textContentType: .password, isSecured: true, user: $userModel)
                .padding(EdgeInsets(top: 27, leading: 27, bottom: 0, trailing: 0))
        }
    }
}

struct LoginInputView_Previews: PreviewProvider {
    static var previews: some View {
        LoginInputView(userModel: .constant(UserModel(email: "", password: "")))
    }
}
