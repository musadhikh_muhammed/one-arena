//
//  ListPickerView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 1/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

struct ListPickerView<T: Listable>: View {
    @Environment (\.presentationMode) private var presenting
    
    var items: [T]
    var title: String
    
    @Binding private(set) var selected: T?
    
    var body: some View {
        ZStack {
            Color.white
            VStack {
                HStack {
                    Text(title)
                        .font(.system(size: 18, weight: .bold))
                        .padding()
                    Spacer()
                }
                List(items) { item in
                    Button(action: {
                        selected = item
                        self.presenting.wrappedValue.dismiss()
                    }, label: {
                        HStack {
                            AsyncImage(url: item.thumbnailUrl) {
                                Image(systemName: "photo")
                                    .resizable()
                            }
                            .scaledToFill()
                            .frame(width: 50)
                            Text(item.title)
                        }
                    })
                }
            }
        }
    }
}


#if DEBUG
struct ListPickerView_Previews: PreviewProvider {
    static var previews: some View {
        ListPickerView(items: [Game(id: "1", title: "PubG", thumbnailUrl: "s1"), Game(id: "2", title: "CAndy Crush", thumbnailUrl: "s2")], title: "", selected: .constant(Game(id: "1", title: "PubG", thumbnailUrl: "s1")))
    }
}
#endif

