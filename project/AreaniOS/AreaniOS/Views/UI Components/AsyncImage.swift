//
//  AsyncImage.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 3/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import Combine
import Foundation

protocol ImageCachable {
    subscript (url: URL?) -> UIImage? { get set }
}

struct ImageCache: ImageCachable {
    private let cache = NSCache<NSURL, UIImage>()
    
    subscript(url: URL?) -> UIImage? {
        get {
            guard let key = url as NSURL? else {
                return nil
            }
            return cache.object(forKey: key)
        }
        set {
            guard let key = url as NSURL? else {
                return
            }
            newValue == nil ? cache.removeObject(forKey: key) : cache.setObject(newValue!, forKey: key)
        }
    }
}


struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCachable = ImageCache()
}

extension EnvironmentValues {
    var imageCache: ImageCachable {
        get {
            self[ImageCacheKey.self]
        }
        set{
            self[ImageCacheKey.self] = newValue
        }
    }
}

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    private let url: URL?
    private var cancellable: AnyCancellable?
    private var cache: ImageCachable?
    
    init(url: String?, cache: ImageCachable? = nil) {
        self.url = url?.url
        self.cache = cache
    }
    
    deinit {
        cancel()
    }
    
    func load() {
        guard let url = self.url else {
            image = nil
            return
        }
        
        if let image  = cache?[url] {
            self.image = image
            return
        }
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data ) }
            .replaceError(with: nil)
            .handleEvents(receiveOutput: {[weak self] in self?.cache($0) } )
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in self?.image  = $0 }
        
            
    }
    
    private func cache(_ image: UIImage?) {
        guard let u = url else {
            return
        }
        image.map { cache?[u] = $0 }
    }
    
    func cancel() {
        cancellable?.cancel()
    }
}


struct AsyncImage<T: View>: View {
    @StateObject private var loader: ImageLoader
    private let placeholder: T
    
    init(url: String?, @ViewBuilder placeholder: () -> T) {
        self.placeholder = placeholder()
        _loader = StateObject(wrappedValue: ImageLoader(url: url, cache: Environment(\.imageCache).wrappedValue))
    }
    
    var body: some View {
        content
            .onAppear(perform: loader.load)
    }
    
    private var content: some View {
        Group {
            if let image = loader.image  {
                Image(uiImage: image)
                    .resizable()
            } else {
                placeholder
            }
        }
    }
}

struct AsyncImage_Previews: PreviewProvider {
    static var previews: some View {
        AsyncImage(url: nil) {
            Text("")
        }
    }
}
