//
//  NavigationBarView.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

enum  NavigationBarButtonActionType {
    case none
    case search
    case notification
}
struct NavigationBarView: View {
//    var searchAction: () -> Void
//    var notificationAction: () -> Void
    
    @Binding var barButtonActionType: NavigationBarButtonActionType
    
    var body: some View {
        ZStack {
            Color.white
            VStack {
                HStack {}
                .frame(height:80)
                HStack {
                    Spacer()
                    NavigationLink(
                        destination: Text("Destination"),
                        label: {
                            Button(action: searchButtonAction) {
                                Image("SearchIcon")
                            }
                        })
                    
                    .frame(width: 30, height: 30)
                    .padding(10)
                    NavigationLink(
                        destination: ChallengeNotificationListView(),
                        label: {
                            
                            Button(action: notificationButtonAction) {
                                Image("NotificationIcon")
                            }
                            .frame(width: 30, height: 30)
                            .padding(.trailing,10)
                        })
                }
                .frame(height: 40)
                
                HStack {
                    Text("One Arena")
                        .font(.system(size: 32, weight: .bold, design: .default))
                        .padding(.leading)
                    Spacer()
                }
                .frame(height:40)
                Spacer()
            }
        }
        .frame(height:100)
        .edgesIgnoringSafeArea(.top)
    }
    
    private func searchButtonAction() {
        self.barButtonActionType = .search
    }
    
    private func notificationButtonAction() {
        self.barButtonActionType = .notification
    }
}

struct NavigationBarView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBarView(barButtonActionType: .constant(.none))
    }
}
