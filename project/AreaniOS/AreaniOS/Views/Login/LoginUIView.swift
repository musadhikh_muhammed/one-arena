//
//  LoginUIView.swift
//  AreaniOS
//
//  Created by Chan Pyae Aung on 23/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct LoginUIView: View {
    
    @State private var userModel: UserModel = UserModel(email: "", password: "")
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                Text("Login")
                    .font(.system(size: 38))
                    .fontWeight(.bold)
                Spacer()
            }.padding(EdgeInsets(top: 0, leading: 27, bottom: 0, trailing: 0))
            
            LoginInputView(userModel: $userModel)
                        
            Button(action: {
                login()
            }) {
                Text("Login")
                    .convertBlueGradientButton()
            }.padding(EdgeInsets(top: 27, leading: 27, bottom: 0, trailing: 27))
            
            HStack {
                Spacer()
                Button(action: {
                    AppManager.shared.appState = .loggedin
                }, label: {
                    Text("Skip")
                })
                Spacer()
            }
            .padding(.top, 40)
            
        }
    }
    
    func login() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        FireStoreDB.storeDB.login(email: userModel.email, password: userModel.password) { isSucces in
            UserDefaults.standard.setValue(isSucces, forKey: "isLoggedin")
            DispatchQueue.main.async {
                AppManager.shared.appState = .loggedin
            }
        }
    }
}

struct LoginUIView_Previews: PreviewProvider {
    static var previews: some View {
        LoginUIView()
    }
}

fileprivate extension LinearGradient {
    static let actionButton = LinearGradient(gradient: Gradient(colors: [Color(red: 0, green: 161/255, blue: 1, opacity: 1), Color(red: 0, green: 115/255, blue: 247/255, opacity: 1)]),
                                               startPoint: .topLeading,
                                               endPoint: .bottomTrailing)
}

struct BlueGradientButton: ViewModifier {
    
    func body(content: Content) -> some View {
        return content
            .foregroundColor(.white)
            .font(.system(size: 17))
            .frame(height: 56)
            .frame(minWidth: 0, maxWidth: .infinity)
            .background(LinearGradient.actionButton)
            .cornerRadius(10)
    }
    
}
