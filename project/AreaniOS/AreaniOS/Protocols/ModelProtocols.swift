//
//  ModelProtocols.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 1/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

protocol Listable where Self: Identifiable{
    var title: String {get}
    var thumbnailUrl: String {get}
}
