//
//  ChallengeListModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 27/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

struct ChallengeNotifcationList: Codable {
    var challenges: [ChallengNotification]
}

struct ChallengNotification: Codable, Identifiable {
    var id: String
    var challengedBy: String
    var createdAt: String
    var shortDescription: String
    var description: String
    var image: String
}
