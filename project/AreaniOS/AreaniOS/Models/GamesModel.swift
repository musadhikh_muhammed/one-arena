//
//  GamesModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 2/2/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

struct Game: Codable, Identifiable {
    var id: String
    var title: String
    var thumbnailUrl: String
}

extension Game: Listable {}

struct ProsModel: Codable, Identifiable {
    var id: String
    var name: String
    var thumbnailUrl: String
}

extension ProsModel: Listable {
    var title: String {
        return name
    }
}


struct ChallengeCreated: Codable, Identifiable {
    var id: String
    var createBy: String
    var challengedTo: String
    var eventDate: String
    var eventTime: String
    var gameTitle: String
    var gameId: String
    var gameThumbnailUrl: String
    var intro: String
    var status: ChallengeStatus
    var documentID: String
    var challengedName: String
}

enum ChallengeStatus: String, Codable {
    case unread
    case pending
    case accepted
    case declined
    case cancelled
}
