//
//  RailModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

enum RailType: String, Codable {
    case hero
    case swimlane
    case category
}

enum ChallengeType: String, Codable {
    case open
    case waiting
    case upcoming
}

struct Page: Codable {
    var rails: [Rail]
}
struct Rail: Codable, Identifiable {
    var id: String
    var title: String
    var type: RailType
    var items: [RailItem]
}

struct RailItem: Codable, Identifiable {
    var id: String
    var title: String
    var image: String
    var upComingChallenges: [Challenges]
    var waitingChallenges: [Challenges]
    var openChallenges: [Challenges]
}

struct Challenges: Codable, Identifiable {
    var id: String
    var date: String
    var fanName: String
    var proName: String
    var price: String
    var challengeType: ChallengeType
}
