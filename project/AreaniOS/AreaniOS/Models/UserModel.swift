//
//  UserModel.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 23/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

struct UserModel {
    var email: String
    var password: String
}

protocol Person {
    var id: String { get }
    var email: String { get }
    var firstName: String { get }
    var lastName: String { get }
    var rating: String { get }
    var userType: String { get }
    var imageUrl: String { get }
}

struct User: Codable, Identifiable {
    var id: String
    var email: String
    var firstName: String
    var lastName: String
    var rating: String
    var userType: String
    var imageUrl: String
}

extension User: Person {}


struct Player: Codable, Identifiable {
    var id: String
    var email: String
    var firstName: String
    var lastName: String
    var rating: String
    var userType: String
    var imageUrl: String
}

extension Player: Person {}
extension Player: Listable {
    var title: String {
        return firstName.appending(" ").appending(lastName)
    }
    
    var thumbnailUrl: String {
        return imageUrl
    }
}


enum UserType: String, Codable {
    case pro
    case fan
    case streamer
}
