//
//  JSONCodable.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation

struct JSONCodable {
    static func decode<T: Codable>(data: Data, type: T.Type) -> Result<T, Error> {
        do {
            let result = try JSONDecoder().decode(type, from: data)
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func encode<T: Codable>(data: T) -> Result<Data, Error> {
        do {
            let k = try JSONEncoder().encode(data)
            return .success(k)
            
        } catch let err {
            print(err)
            return .failure(err)
        }
    }
    
    static func convertToReadable<T: Codable>(data: Any, to type: T.Type) -> Result<T, Error> {
        let k = serialize(data: data)
        switch k {
        case .success(let resultData): return decode(data: resultData, type: T.self)
        case .failure(let err): return .failure(err)
        }
    }
    
    static func convertToReadable<T: Codable, U: Codable>(data: T, to type: U.Type) -> Result<U, Error> {
        let k = encode(data: data)
        switch k {
        case .success(let resultData): return decode(data: resultData, type: U.self)
        case .failure(let err): return .failure(err)
        }
    }
    
    static func serialize(data: Any) -> Result<Data, Error> {
        do {
            let res = try JSONSerialization.data(withJSONObject: data, options: .fragmentsAllowed)
            return .success(res)
        } catch let err {
            return .failure(err)
        }
    }
    
}
