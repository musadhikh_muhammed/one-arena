//
//  Colors.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 25/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    static let tabForeground = rgb(248, 248, 248, 0.82)
    static let tabItemTitle = rgb(142, 142, 147)
    static let tabItemTitleSelected = rgb(0, 145, 255)
    static let swimlaneTitle = rgb(77, 77, 77)
    static let challengeTime = rgb(77, 77, 75)
    static let textEditorBG = rgb(216, 216, 216)
    
    
    static func rgb(_ r: Double, _ g:Double, _ b:Double, _ a: Double = 1) -> Color {
        return Color(.sRGB, red: r/255.0, green: g/255.0, blue: b/255.0, opacity: a)
    }
}

extension UIColor {
    static let tabBarTintColor = rgb(248, 248, 248, 0.82)
    static let tabItemColor = rgb(142, 142, 147)
    static let tabItemSelectedColor = rgb(0, 145, 255)
    
    static func rgb(_ r: CGFloat, _ g:CGFloat, _ b:CGFloat, _ a: CGFloat = 1) -> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
}


extension String {
    var url: URL? {
        return URL(string: self)
    }
}
