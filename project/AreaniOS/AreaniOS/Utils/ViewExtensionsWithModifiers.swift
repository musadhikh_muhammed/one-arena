//
//  ViewExtensionsWithModifiers.swift
//  AreaniOS
//
//  Created by Musadhikh Muhammed K on 28/1/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI

extension View {
    func convertBlueGradientButton() -> some View {
        self.modifier(BlueGradientButton())
    }
}
